package main
import "fmt"
//create a function that takes in the string as a parameter and returns the string


func reverse(str string) (reversed string) {
	for _, v := range str {
		//reverse the string
		reversed = string(v) + reversed
	}
	return reversed
}

func main() {
	fmt.Println(reverse("Sendy"))
}