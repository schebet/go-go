package main
import "fmt"
type artists []string
func main() {
	BTS := artists{"JK", "RM", "V", "Jin", "J-Hope", "Suga", "Jimin"}
	BTS.print()
}
func (a artists) print() {
	for i, artist := range a {
		fmt.Println(i, artist)
	}
}