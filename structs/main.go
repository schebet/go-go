package main

import (
	"fmt"
	"strconv"
)




//define person struct

type  Person struct {
	firstName string
	age int
}

//greeting method-value receiver
func (p Person)greet() string{
	return "hello i'm " +p.firstName + " i'm " +strconv.Itoa(p.age)
}

//pointer receiver
func(p *Person)hasBirthday(){
	p.age++
}

func main()  {
	person1 :=Person{firstName: "stace", age: 20}

	//fmt.Println(person1)
	//fmt.Println(person1.firstName)
	//
	//person1.firstName = "Ann"
	//fmt.Println(person1)
	person1.hasBirthday()
	fmt.Println(person1.greet())
}
