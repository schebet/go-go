package main

import (
	"fmt"
	"sort"
)
//create a function that takes the array as a parameter
//define two variables to rep the largest and second largest number in the array
//
//func secondLarge(nums []int) int{
//	largest := 0
//	secondLargest := 0
//
//	largest = nums[0]
//	for i := 1; i <= 4; i++ {
//		if largest < nums[i] {
//			secondLargest = largest
//			largest = nums[i]
//		} else if secondLargest < nums[i] {
//			secondLargest = nums[i]
//		}
//	}
//	return secondLargest
//}

func main() {
	var nums =  []int{43, 11, 6, 81, 34}
	sort.Ints(nums)
	fmt.Println(nums[len(nums)-2])
}