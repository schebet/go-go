package main

import (
	"fmt"
	"strings"
)
//create a function that takes the array as a parameter
//loop through each item of the array
//slice the first letter of the name and check whether it's J
//if it's J append the name to the result slice
func sameLetter(names []string) []string{
	var result []string
	for i:=0; i<len(names); i++{
		firstLetter:=names[i][0:1]
		if strings.ToLower(firstLetter) == "j"{
			result = append(result, names[i])
		}
	}
	return result
}
func main() {
	var names = []string{"Joseph", "Clinton", "Justin", "Mark"}
	fmt.Println(sameLetter(names))
}