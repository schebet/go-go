package main
import "fmt"

//create a function that takes the array as a parameter
//initialize a map that has an integer as the key and a boolean as value
//loop through the array and add to an item the map if it doesn't exist
//if the item exists return the item
func findDuplicate(arr []int) int{
	seen := make(map[int]bool)
	for i:=0; i<len(arr); i++{
		if seen[arr[i]] == true{
			return arr[i]
		} else {
			seen[arr[i]] = true
		}
	}
	return -1
}

func main(){
	fmt.Println(findDuplicate([]int{4, 1, 6, 8, 4}))
}