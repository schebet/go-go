package main

import "fmt"

//
func reverse(str string) (reversed string) {
	for _, v := range str {
		reversed = string(v) + reversed
	}
	return reversed
}

func main() {
	fmt.Println(reverse("Sendy"))
}