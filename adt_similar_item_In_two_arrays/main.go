package main

import "fmt"
//create a function that takes in the two arrays as parameters
//perform a for nested loop on both arrays - outer loop iterated the first while the inner loop
//iterates the second array while checking for similar values
//add a break operation to stop the loop once the common value is found
func commonItem(first, second []string) string {

	var common string
	for i := 0;
		i < len(first);
		i++ {
		for j := 0;
			j < len(second);
		j++ {
			if first[i] == second[j] {
				common = first[i]
				break

			}
		}
	}
	return common
}

func main() {
	first := []string{"CSS", "HTML", "JavaScript", "Vue.js"}
	second := []string{"Java", "Go", "JavaScript", "PHP"}

	fmt.Println(commonItem(first, second))

}
